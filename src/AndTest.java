import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.Test;

public class AndTest {

	private double[] wagi = new double[3];
	private double biasAbs;
	@Before
	public void before(){
		Main.main(null);
		wczytajWagi();
		biasAbs = Math.abs(wagi[0]);
	}

	@Test
	public void biasGreaterThan1() {
		assertTrue(biasAbs>wagi[1]);
	}
	@Test
	public void biasGreaterThan2() {
		assertTrue(biasAbs>wagi[2]);
	}
	@Test
	public void biasSmallerThanSum() {
		assertTrue(biasAbs<wagi[1]+wagi[2]);
	}

	private void wczytajWagi() {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(Params.PLIK_WAGI));
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray array = (JSONArray) jsonObject.get(Neuron.WAGI_NAME);
			Iterator<Double> iterator = array.iterator();
			int i = 0;
			while (iterator.hasNext()) {
				wagi[i++] = iterator.next();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}

	}

}
