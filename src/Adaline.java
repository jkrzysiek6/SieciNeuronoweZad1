import java.util.Arrays;
import java.util.Collections;

public class Adaline extends Neuron {

	@Override
	protected boolean epoka() {
		Collections.shuffle(Arrays.asList(Params.learningSet));
		double bladSredniokwadratowy = 0;
		for (int nrWzorca = 0; nrWzorca < Params.learningSet.length; nrWzorca++) {
			wprowadzWektorWej(nrWzorca);
			double bladNaWyjsciu = przetworzWzorzecUczacy(wartosci, nrWzorca);
			bladSredniokwadratowy += Math.pow(bladNaWyjsciu, 2);
		}
		bladSredniokwadratowy /= Params.learningSet.length;
		return bladSredniokwadratowy < Params.getDopuszczalnyBlad();
	}

	@Override
	protected double przetworzWzorzecUczacy(double[] wartosci, int nrWzorca) {
		double net = obliczCalkowitePobudzenie(wartosci);
		int wartOcz = Params.learningSet[nrWzorca].getoZ();
		double bladNaWyjsciu = obliczBladNaWyjsciu(net, wartOcz);
		skorygujWagi(wartosci, bladNaWyjsciu);
		return bladNaWyjsciu;
	}

	protected double obliczBladNaWyjsciu(double wyjscie, int wartOcz) {
		return wartOcz - wyjscie;
	}

	protected void skorygujWagi(double[] wartosci, double bladNaWyjsciu) {
		for (int i = 0; i < wagi.length; i++) {
			wagi[i] = wagi[i] + Params.getWspUcz() * bladNaWyjsciu * wartosci[i];
		}
	}

}
