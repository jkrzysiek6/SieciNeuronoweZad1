import java.math.BigDecimal;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

public class Wykres extends ApplicationFrame{

	public Wykres(List<Integer> czasyUczenia, List<Double> wartosciParam, String tytul, String podpisX, String podpisY){
		super("tytul");
        JFreeChart lineChart = ChartFactory.createLineChart(
                tytul,
                podpisX,
                podpisY,
                createDataset(czasyUczenia, wartosciParam),
                PlotOrientation.VERTICAL,
                false, true, false);

        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
        setContentPane(chartPanel);
	}

    private DefaultCategoryDataset createDataset(List<Integer> czasyUczenia, List<Double> wartosciParam) {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i<czasyUczenia.size(); i++) {
        	Double wartParam = (double) Math.round(wartosciParam.get(i) * 100) / 100;
        	dataset.addValue(BigDecimal.valueOf(czasyUczenia.get(i)), "a", wartParam);
		}
        return dataset;

    }
    public static void display(List<Integer> czasyUczenia, List<Double> wartosciParam, String tytul, String podpisX, String podpisY) {
        Wykres chart = new Wykres(czasyUczenia, wartosciParam, tytul, podpisX, podpisY);
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
    }
}