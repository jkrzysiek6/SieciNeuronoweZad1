
public class Input {
double value, weight;

public Input(double value, double weight) {
	super();
	this.value = value;
	this.weight = weight;
}

public double getValue() {
	return value;
}

public void setValue(double value) {
	this.value = value;
}

public double getWeight() {
	return weight;
}

public void setWeight(double weight) {
	this.weight = weight;
}

}
