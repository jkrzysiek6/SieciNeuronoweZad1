import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

	private static List<Integer> czasyUczenia;
	private static List<Double> wartosciParam;

	public static void main(String[] args) {
		Params.initializeLearingSet();
		Params.wczytajParametryZJsona(0);

		Neuron neuron;
		if (Params.isAdaline()) {
			neuron = new Adaline();
		} else {
			neuron = new Perceptron();
		}

		if (Params.isRun()) {
			neuron.run();
		} else {
			neuron.naucz();
		}

		if (Params.czyBadacWspUcz()) {
			badajWspUcz(neuron);
			Wykres.display(czasyUczenia, wartosciParam, "Badanie wplywu wsp. uczenia na liczbe epok", "Wartosci wsp. uczenia", "Liczba epok");
		}
		if(Params.getBadajZakresyWagPocz()){
			badajZakresyWagPocz(neuron);
			Wykres.display(czasyUczenia, wartosciParam, "Badanie wplywu zakresu wag poczatkowych na liczbe epok", "Wartosci zakresu wag pocz. (-x, x)", "Liczba epok");
		}
	}

	private static void badajWspUcz(Neuron neuron) {
		czasyUczenia = new ArrayList<>();
		wartosciParam = new ArrayList<>();
		int i = 0;
		while (Params.getWspUcz() <= Params.getWspUczMax()) {
			Params.wczytajParametryZJsona(i);
			int srLiczbaEpok = badajNRazy(neuron, 10);
			czasyUczenia.add(srLiczbaEpok);
			wartosciParam.add(Params.getWspUcz());
			i++;
		}
	}

	private static void badajZakresyWagPocz(Neuron neuron) {
		czasyUczenia = new ArrayList<>();
		wartosciParam = new ArrayList<>();
		int i = 0;
		while (Params.getZakrWagPoczAbs() >= Params.getZakrWagPoczAbsMin()) {
			Params.wczytajParametryZJsona(i);
			int srLiczbaEpok = badajNRazy(neuron, 10);
			czasyUczenia.add(srLiczbaEpok);
			wartosciParam.add(Params.getZakrWagPoczAbs());
			i++;
		}
	}

	private static int badajNRazy(Neuron neuron, int n) {
		int srLiczbaEpok = 0;
		for (int i = 0; i < n; i++) {
			srLiczbaEpok += neuron.naucz();
		}
		return srLiczbaEpok /= n;
	}

}
