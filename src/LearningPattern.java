
public class LearningPattern {
	private double x1, x2;
	private int oZ;

	public LearningPattern(double x1, double x2, int oZ) {
		super();
		this.x1 = x1;
		this.x2 = x2;
		this.oZ = oZ;
	}

	public double getX1() {
		return x1;
	}

	public double getX2() {
		return x2;
	}

	public int getoZ() {
		return oZ;
	}

}
