import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public abstract class Neuron {
	public static final String WAGI_NAME = "wagi";

	protected double[] wagi = new double[3];
	protected double[] wartosci = new double[3];

	public void run() {
		wczytajWagi();
		double[] wartTest = { 1.0, Params.getTestValue1(), Params.getTestValue2() };
		double net = obliczCalkowitePobudzenie(wartTest);
		int wyjscie = funkcjaProgu(net);
		System.out.println(wyjscie);
	}

	public int naucz() {
		boolean warunekStop;
		int liczbaEpok = 0;
		if (Params.czyLosowacWagiPocz()) {
			losujWagiPoczatkowe();
		}else{
			wczytajWagiPoczatkowe();
		}
		do {
			warunekStop = epoka();
			liczbaEpok++;
			if(liczbaEpok == 50){
				break;
			}
		} while (!warunekStop);
		zapiszWagiDoPliku();
		System.out.println("Zakres wag pocz: "+Params.getZakrWagPoczAbs());
		System.out.println("Wsp ucz: " + Params.getWspUcz());
		System.out.println("Liczba epok: " + liczbaEpok);
		drukujWagi(wagi);
		System.out.println("\n");
		return liczbaEpok;
	}

	abstract protected boolean epoka();

	protected void wprowadzWektorWej(int nrWzorca) {
		wartosci[0] = 1;
		wartosci[1] = Params.learningSet[nrWzorca].getX1();
		wartosci[2] = Params.learningSet[nrWzorca].getX2();
	}

	abstract protected double przetworzWzorzecUczacy(double[] wartosci, int nrWzorca);

	protected double obliczCalkowitePobudzenie(double[] wartosci) {
		double net = 0;
		for (int i = 0; i < wagi.length; i++) {
			net += wartosci[i] * wagi[i];
		}
		return net;
	}

	protected int funkcjaProgu(double zwroconaWartosc) {
		return zwroconaWartosc > Params.getProg() ? 1 : Params.getFunkcjaBiUniPolarna();
	}

	private void losujWagiPoczatkowe() {
		for (int i = 0; i < wagi.length; i++) {
			wagi[i] = ThreadLocalRandom.current().nextDouble(0-Params.getZakrWagPoczAbs(), Params.getZakrWagPoczAbs());
		}
	}

	private void wczytajWagiPoczatkowe() {
		wagi[0] = Params.getBias();
		wagi[1] = Params.getWaga1();
		wagi[2] = Params.getWaga2();
	}

	private void zapiszWagiDoPliku() {
		JSONObject jsonObj = new JSONObject();
		JSONArray array = new JSONArray();
		List<Double> listaWag = new ArrayList();
		for (int i = 0; i < wagi.length; i++) {
			listaWag.add(wagi[i]);
		}
		jsonObj.put(WAGI_NAME, listaWag);
		try {
			FileWriter file = new FileWriter(Params.PLIK_WAGI);
			file.write(jsonObj.toJSONString());
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void wczytajWagi() {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(Params.PLIK_WAGI));
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray msg = (JSONArray) jsonObject.get(WAGI_NAME);
			Iterator<Double> iterator = msg.iterator();
			int i = 0;
			while (iterator.hasNext()) {
				wagi[i++] = iterator.next();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}

	}

	private void drukujWagi(double[] wagi) {
		for (int i = 0; i < wagi.length; i++) {
			System.out.println("waga " + i + " = " + wagi[i]);
		}
	}
}