import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import lombok.Getter;

@Getter
public class Params {
	public static LearningPattern[] learningSet = new LearningPattern[4];

	private static final String IS_ADALINE_NAME = "IS_ADALINE";
	private static final String WSP_UCZ_NAME = "WSP_UCZ";
	private static final String WAGA_POCZ_ABS_MIN_NAME = "WAGA_POCZ_ABS_MIN";
	private static final String WAGA_POCZ_ABS_MAX_NAME = "WAGA_POCZ_ABS_MAX";
	private static final String WAGA_POCZ_DIFF_NAME = "WAGA_POCZ_DIFF";

	private static final String RUN_NAME = "RUN";
	private static final String TEST_VALUE_1_NAME = "TEST_VALUE_1";
	private static final String TEST_VALUE_2_NAME = "TEST_VALUE_2";
	private static final String DOPUSZCZALNY_BLAD_NAME = "DOPUSZCZALNY_BLAD";
	private static final String PROG_NAME = "PROG";

	private static final String LOSUJ_WAGI_POCZ_NAME = "LOSUJ_WAGI_POCZ";
	private static final String BIAS_POCZ_NAME = "BIAS_POCZ";
	private static final String WAGA_POCZ_1_NAME = "WAGA_POCZ_1";
	private static final String WAGA_POCZ_2_NAME = "WAGA_POCZ_2";
	private static final String BADAJ_WSP_UCZ_NAME = "BADAJ_WSP_UCZ";
	private static final String WSP_UCZ_DIFF = "WSP_UCZ_DIFF";
	private static final String WSP_UCZ_MAX = "WSP_UCZ_MAX";
	private static final String BADAJ_ZAKRESY_WAG_POCZ_NAME = "BADAJ_ZAKRESY_WAG_POCZ";
	private static final String FUNKCJA_BI_UNI_POLARNA_NAME = "FUNKCJA_BI_UNI_POLARNA";

	public static final String PLIK_PARAMS = "parametry.json";
	public static final String PLIK_WAGI = "wagi.json";

	private static Integer funkcjaBiUniPolarna = null;

	private static Boolean isAdaline = null, run = null, losujWagi = null, badajWspUcz = null,
			badajZakresyWagPocz = null;

	private static Double wspUcz = null, prog = null, dopuszczalnyBlad = null, zakrWagPoczAbsMin = null,
			zakrWagPoczAbsMax = null, testValue1 = null, testValue2 = null, bias = null, waga1 = null, waga2 = null,
			wspUczDiff = null, wspUczMax = null, zakrWagPoczAbs = null;

	protected static void wczytajParametryZJsona(int i) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader(PLIK_PARAMS));
			JSONObject jsonObject = (JSONObject) obj;


			isAdaline = (Boolean) jsonObject.get(IS_ADALINE_NAME);
			zakrWagPoczAbsMin = (Double) jsonObject.get(WAGA_POCZ_ABS_MIN_NAME);
			zakrWagPoczAbsMax = (Double) jsonObject.get(WAGA_POCZ_ABS_MAX_NAME);
			run = (Boolean) jsonObject.get(RUN_NAME);
			testValue1 = (Double) jsonObject.get(TEST_VALUE_1_NAME);
			testValue2 = (Double) jsonObject.get(TEST_VALUE_2_NAME);
			dopuszczalnyBlad = (Double) jsonObject.get(DOPUSZCZALNY_BLAD_NAME);
			badajZakresyWagPocz = (Boolean) jsonObject.get(BADAJ_ZAKRESY_WAG_POCZ_NAME);

			wczytajWspUczenia(i, jsonObject);

			wczytajZakresyWagPocz(i, jsonObject);

			losujWagi = (Boolean) jsonObject.get(LOSUJ_WAGI_POCZ_NAME);
			if (!losujWagi) {
				bias = (Double) jsonObject.get(BIAS_POCZ_NAME);
				waga1 = (Double) jsonObject.get(WAGA_POCZ_1_NAME);
				waga2 = (Double) jsonObject.get(WAGA_POCZ_2_NAME);
			}

			if (isAdaline) {
				prog = (Double) jsonObject.get(PROG_NAME);
			} else {
				prog = 0.0;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}

	public static void initializeLearingSet() {
		JSONParser parser = new JSONParser();
			Object obj;
			try {
				obj = parser.parse(new FileReader(PLIK_PARAMS));
				JSONObject jsonObject = (JSONObject) obj;
				funkcjaBiUniPolarna = ((Long) jsonObject.get(FUNKCJA_BI_UNI_POLARNA_NAME)).intValue();

				learningSet[0] = new LearningPattern(funkcjaBiUniPolarna, 1, funkcjaBiUniPolarna);
				learningSet[1] = new LearningPattern(1, funkcjaBiUniPolarna, funkcjaBiUniPolarna);
				learningSet[2] = new LearningPattern(funkcjaBiUniPolarna, funkcjaBiUniPolarna, funkcjaBiUniPolarna);
				learningSet[3] = new LearningPattern(1, 1, 1);
			} catch (IOException | ParseException e) {
				e.printStackTrace();
			}

	}

	private static void wczytajWspUczenia(int i, JSONObject jsonObject) {
		wspUcz = (Double) jsonObject.get(WSP_UCZ_NAME);
		badajWspUcz = (Boolean) jsonObject.get(BADAJ_WSP_UCZ_NAME);
		if (badajWspUcz) {
			wspUczDiff = (Double) jsonObject.get(WSP_UCZ_DIFF);
			wspUcz += (i * wspUczDiff);
			wspUczMax = (Double) jsonObject.get(WSP_UCZ_MAX);
		}
	}

	private static void wczytajZakresyWagPocz(int i, JSONObject jsonObject) {
		zakrWagPoczAbs = (Double) jsonObject.get(WAGA_POCZ_ABS_MAX_NAME);
		if (badajZakresyWagPocz) {
			zakrWagPoczAbs -= (i * (Double) jsonObject.get(WAGA_POCZ_DIFF_NAME));
		}
	}

	public static Boolean isAdaline() {
		return isAdaline;
	}

	public static Double getWspUcz() {
		return wspUcz;
	}

	public static Double getZakrWagPoczAbsMin() {
		return zakrWagPoczAbsMin;
	}

	public static Double getZakrWagPoczAbsMax() {
		return zakrWagPoczAbsMax;
	}

	public static Boolean isRun() {
		return run;
	}

	public static Double getTestValue1() {
		return testValue1;
	}

	public static Double getTestValue2() {
		return testValue2;
	}

	public static Double getDopuszczalnyBlad() {
		return dopuszczalnyBlad;
	}

	public static Double getProg() {
		return prog;
	}

	public static Double getBias() {
		return bias;
	}

	public static Double getWaga1() {
		return waga1;
	}

	public static Double getWaga2() {
		return waga2;
	}

	public static Double getWspUczMax() {
		return wspUczMax;
	}

	public static Boolean czyBadacWspUcz() {
		return badajWspUcz;
	}

	public static Boolean czyLosowacWagiPocz() {
		return losujWagi;
	}

	public static Boolean getBadajZakresyWagPocz() {
		return badajZakresyWagPocz;
	}

	public static Double getZakrWagPoczAbs() {
		return zakrWagPoczAbs;
	}

	public static Integer getFunkcjaBiUniPolarna() {
		return funkcjaBiUniPolarna;
	}
}
