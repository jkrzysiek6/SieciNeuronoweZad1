import java.util.Arrays;
import java.util.Collections;

public class Perceptron extends Neuron {

	@Override
	protected boolean epoka() {
		Collections.shuffle(Arrays.asList(Params.learningSet));
		boolean wszystkieBledyZerowe = true;
		for (int nrWzorca = 0; nrWzorca < Params.learningSet.length; nrWzorca++) {
			wprowadzWektorWej(nrWzorca);
			double bladNaWyjsciu = przetworzWzorzecUczacy(wartosci, nrWzorca);
			wszystkieBledyZerowe &= (bladNaWyjsciu == 0);
		}
		return wszystkieBledyZerowe;
	}

	@Override
	protected double przetworzWzorzecUczacy(double[] wartosci, int nrWzorca) {
		double net = obliczCalkowitePobudzenie(wartosci);
		int wyjscie = funkcjaProgu(net);
		int bladNaWyjsciu = obliczBladNaWyjsciu(wyjscie, Params.learningSet[nrWzorca].getoZ());
		if (bladNaWyjsciu != 0) {
			skorygujWagi(wartosci, bladNaWyjsciu);
		}
		return bladNaWyjsciu;
	}

	protected int obliczBladNaWyjsciu(int wyjscie, int wartOcz) {
		return wartOcz - wyjscie;
	}

	protected double[] skorygujWagi(double[] wartosci, int bladNaWyjsciu) {
		for (int i = 0; i < wagi.length; i++) {
			wagi[i] = wagi[i] + Params.getWspUcz() * bladNaWyjsciu * wartosci[i];
		}
		return wagi;
	}

}
